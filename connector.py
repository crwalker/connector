#!/usr/bin/env python

import sys
import socket

from twisted.web.static import File
from twisted.web.server import Site
from twisted.internet import reactor
from twisted.python import log
from twisted.internet.protocol import Protocol, DatagramProtocol, Factory, ClientFactory, ReconnectingClientFactory

from autobahn.twisted.websocket import WebSocketServerFactory, WebSocketServerProtocol, listenWS
from autobahn.twisted.resource import WebSocketResource

# Obviously this is a stupid implementation but it works sufficiently well


# class NodeClientTCP(Protocol):
#     def __init__(self, factory):
#         self.factory = factory

#     def connectionMade(self):
#         self.transport.write(self.factory.message)
#         self.transport.loseConnection()


# class NodeClientTCPFactory(ClientFactory):
#     def __init__(self, message):
#         self.message = message

#     def startedConnecting(self, connector):
#         print 'Started to connect.'

#     def buildProtocol(self, addr):
#         print 'Connected.'
#         return NodeClientTCP(self)

#     def clientConnectionLost(self, connector, reason):
#         print 'Lost connection.  Reason:', reason

#     def clientConnectionFailed(self, connector, reason):
#         print 'Connection failed. Reason:', reason


class NodeUDP(DatagramProtocol):
    def __init__(self, connector):
        print "init NodeUDP"
        self.connector = connector

    # def sendMessage(self, data, (host, port)):
    #     #pass
    #     self.transport.write(data, (host, port))

    def datagramReceived(self, data, (host, port)):
        print "received %r from %s:%d" % (data, host, port)
        self.transport.write(data, (host, port))
        print "datagram received: telling clients"
        self.connector.tell_clients(data, True)


class NodeTCP(Protocol):
    def __init__(self, factory):
        print "init NodeTCP"
        self.factory = factory

    def connectionMade(self):
        print "tcp connection made"
        # ip, port = self.xmlstream.transport.socket.getpeername()
        # print(ip, port)
        # self.factory.add_peer(ip, port)

    def dataReceived(self, message):
        print "tcp received %r" % (message)
        # self.transport.write(data)
        # self.factory.connector.tell_nodes(message)
        self.factory.connector.tell_clients(message, True)


class NodeTCPFactory(Factory):
    def __init__(self, connector):
        self.connector = connector

    def buildProtocol(self, address):
        print 'Building node tcp factory protocol with address {}'.format(address)
        return NodeTCP(self)

    # def add_peer(self, ip, port):
    #     print "add peer ip {}, port {}".format(ip, port)
    #     self.nodes[ip] = {'ip': ip, 'port': port}
    #     # factory.protocol = SomeClientProtocol

    # def remove_peer(self, remote):
    #     print "remove peer {}".format(remote)
    #     self.nodes.pop(remote.peer)


class ClientWS(WebSocketServerProtocol):
    def __init__(self, factory):
        super(ClientWS, self).__init__()
        self.factory = factory

    def onConnect(self, request):
        print("client ws on connect {}".format(request))

    def onOpen(self):
        print("client ws on open {}".format(self))
        self.factory.connector.register_client(self)

    def connectionLost(self, reason):
        # Called when successfully established connection is lost
        print("client ws connection lost {} {}".format(self, reason))
        self.factory.connector.unregister_client(self)

    def onMessage(self, message, isBinary):
        print("on websocket message: message length {} bytes, binary ? {}".format(len(message), isBinary))
        # send message right back to websocket to ensure it looks good
        # self.sendMessage(message, isBinary)
        # # send message to node
        self.factory.connector.tell_nodes(message)


class ClientWSFactory(WebSocketServerFactory):
    #wpass
    def __init__(self, address, connector):
        super(ClientWSFactory, self).__init__(address)
        # self.protocol = ClientWS
        self.connector = connector

    def buildProtocol(self, address):
        print 'Building client ws tcp factory protocol for {}'.format(address)
        return ClientWS(self)


class Connector():
    def __init__(self):
        self.nodes = {
            'default': {
                'stream': {'ip': socket.gethostbyname('sst'), 'port': 8000},
                'datagram': {'ip': socket.gethostbyname('sst'), 'port': 8001},
            },
        }
        self.clients = {}

        self.nodeTCPFactory = NodeTCPFactory(self)
        self.nodeUDPProtocol = NodeUDP(self)
        self.clientFactory = ClientWSFactory(u'ws://127.0.0.1:8012', self)

        self.clientFactory.protocol = ClientWS
        reactor.listenTCP(8010, self.nodeTCPFactory)
        reactor.listenUDP(8011, self.nodeUDPProtocol)
        reactor.listenTCP(8012, self.clientFactory)

        self.tell_nodes('foo')

    def register_client(self, client):
        print "register client {}".format(client)
        self.clients[client.peer] = client

    def unregister_client(self, client):
        print "unregister client {}".format(client)
        self.clients.pop(client.peer)

    def tell_clients(self, message, isBinary):
        print "tell clients {} message: length {}, content {}".format(self.clients, len(message), repr(message))
        for key, client in self.clients.iteritems():
            print( "telling client {}".format(client))
            client.sendMessage(message, isBinary)

    def tell_nodes(self, message):
        print "tell nodes message: length {}, content {}".format(len(message), repr(message))
        for key, node in self.nodes.iteritems():
            self.nodeUDPProtocol.transport.write(
                message,
                (node['datagram']['ip'], node['datagram']['port'])
            )
            # factory = NodeClientTCPFactory(message)
            # reactor.connectTCP(value['ip'], value['port'], factory)

# class NodeTCPFactory(ReconnectingClientFactory):
#     def startedConnecting(self, connector):
#         print 'Started to connect.'

#     def buildProtocol(self, addr):
#         print 'Connected.'
#         print 'Resetting reconnection delay'
#         self.resetDelay()
#         return NodeTCP(self)

#     def clientConnectionLost(self, connector, reason):
#         # Called when successfully established connection is lost
#         print 'Lost connection.  Reason:', reason
#         ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

#     def clientConnectionFailed(self, connector, reason):
#         # Called when connection could not be established
#         print 'Connection failed. Reason:', reason
#         ReconnectingClientFactory.clientConnectionFailed(
#             self,
#             connector,
#             reason
#         )


# class NodeUDP(DatagramProtocol):
#     def datagramReceived(self, data, host, port):
#         print "udp received %r from %s:%d" % (data)
#         # self.transport.write(data, (host, port))
#         connector.tell_clients(data, False)

if __name__ == '__main__':
    connector = Connector()
    log.startLogging(sys.stdout)
    reactor.run()
