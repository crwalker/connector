<img src="logo.png" height="200">
# Connector
Simple connection point between SST nodes and browsers

## Why?
SST uses low level async messages and UDP. Browsers don't.

## Opeation
* set terminal working directory to this directory
* ensure that the Connector constructor sets self.nodes to valid SST nodes
* run `python connector.py` in a terminal
* point the system accessing SST via websocket, e.g. the Concorde web client,  at the address provided to self.ClientWSFactory, e.g. `ws://127.0.0.1:8012`
